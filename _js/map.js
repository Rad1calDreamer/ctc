var map;
function initMap() {
   map = new google.maps.Map(document.getElementById('ctk-map'), {
      center: {
         lat: 53.921689,
         lng: 27.564783
      },
      zoom: 18,
      scrollwheel: false
   });
   var marker = new google.maps.Marker({
      position: {
         lat: 53.921633,
         lng: 27.563657
      },
      map: map,
      icon: '/_img/mapIcon.png'
   });
   marker.setMap(map);
}