/**
 * Created by aa.kornev on 31.05.2016.
 */
var headerMenuBottom;
var headerMenu = document.getElementById('headerMenu');
var sidebarMenu = document.getElementById('sidebarMenu');
function sidebarMenuToggle(){
   headerMenuBottom = headerMenu.getBoundingClientRect();
   headerMenuBottom = headerMenuBottom.bottom;
   var scrolled = window.pageYOffset || document.documentElement.scrollTop;
   sidebarMenu.style.display = (scrolled > headerMenuBottom) ? 'block' : 'none'
}
window.onscroll = function() {
   sidebarMenuToggle();
};


$(document).ready(function() {
   sidebarMenuToggle();
   $('.ctk-reviews').slick({
      prevArrow: $('#revPrev'),
      nextArrow: $('#revNext'),
      centerMode: true,
      initialSlide: 1,
      variableWidth: true,
      centerPadding: '60px',
      dots: true,
      arrows: true,
      dotsClass: 'ctk-review__pager',
      customPaging: function(slider, i) {
         return null;
      },
      autoplay: true,
      autoplaySpeed: 2000
   });
   $('.video-slider').slick({
      centerMode: true,
      initialSlide: 1,
      variableWidth: true,
      dots: false,
      arrows: true
   });

   $('.video-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
      $('.video-slider .slick-slide[data-slick-index='+currentSlide+']').removeClass('showPlay');
   });
   $('.video-slider').on('afterChange', function(slick, currentSlide){
      $('.video-slider .slick-slide[data-slick-index='+currentSlide.currentSlide+']').addClass('showPlay');
   });
});

